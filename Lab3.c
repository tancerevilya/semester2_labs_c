#include <stdio.h>
#include <math.h>

int main()
{
    int s, exit;
    double x1, x2, x, j, f, g, y, i, a, h;

    printf("Vvedite x1=");
    scanf("%lf", &x1);
    printf("Vvedite x2=");
    scanf("%lf", &x2);
    printf("Vvedite a=");
    scanf("%lf", &a);
    printf("Vvedite kolichestvo chagov s=");
    scanf("%d", &s);

    i = (x2 - x1) / s;
    x = x1;

    while (x < x2)
    {
        g = -(10 * (18 * (a * a) + 11 * a * x - 24 * (x * x)) / ((-a * -a) + a * x + 6 * (x * x)));
        printf("G = %lf \n", g);

        h = (3 * (a * a) - 25 * a * x + 8 * (x * x) + 1);
        if (h < 0)
        {
            printf("log ot otrichatelnogo chisla nelzya rasschitat \n");
        }
        else
        {
            y = log(h) / log(10);
            printf("Y = %lf \n", y);
        }

        j = 21 * (a * a) - 34 * a * x + 9 * (x * x);
        if (j > -710.000001, j < 710.000001)
        {
            f = cosh(j);
            printf("F = %e \n", f);
        }
        else
        {
            printf("Cosh nelzya vichislit \n");
        }

        x += i;
        printf("Esli hotite zakonchit, vvedite 2 \n");
        printf("Esli hotite prodoljit, vvedite 1 \n");
        scanf("%d", &exit);
        if (exit == 2)
        {
            break;
        }
    }
    return 0;
}
