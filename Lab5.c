#include <stdio.h>
#include <math.h>

int main()
{
    int s, k, t, s1, l;
    double x1, x2, x, j, f, g, y, i, a, h;
    double mass[10][10];

    printf("Vvedite x1=");
    scanf("%lf", &x1);
    printf("Vvedite x2=");
    scanf("%lf", &x2);
    printf("Vvedite a=");
    scanf("%lf", &a);
    printf("Vvedite kolichestvo chagov s=");
    scanf("%d", &s);

    i = (x2 - x1) / s;
    x = x1;

    k = 0;
    while (x < x2) //подсчет массива
    {
        g = -(10 * (18 * (a * a) + 11 * a * x - 24 * (x * x)) / ((-a * -a) + a * x + 6 * (x * x)));
        mass[k][0] = g;
        //printf("%e ", mass[k][0]);

        h = (3 * (a * a) - 25 * a * x + 8 * (x * x) + 1);
        if (h < 0)
        {
            printf("log ot otrichatelnogo chisla nelzya rasschitat \n");
        }
        else
        {
            y = log(h) / log(10);
            mass[k][1] = y;
            //printf(" %e ", mass[k][1]);
        }

        j = 21 * (a * a) - 34 * a * x + 9 * (x * x);
        if (j > -710.000001 && j < 710.000001)
        {
            f = cosh(j);
            mass[k][2] = f;
            //printf(" %e\n", mass[k][2]);
        }
        else
        {
            printf("Cosh nelzya vichislit \n");
        }
        x += i;
        k++;
    }

    int n = 0, p = 0;
    int b = 0;
    while (p >= n)
    {
        printf("\nVivesti g, press 1\n");
        printf("Vivesti y, press 2\n");
        printf("Vivesti f, press 3\n");
        printf("Vivesti mass, press 4\n");
        printf("Exit --> 5:\n");

        scanf("%d", &b);

        if (b == 1)
        {
            printf("G = %e \n", g);
        }
        else if (b == 2)
        {
            printf("Y = %e \n", y);
        }
        else if (b == 3)
        {
            printf("F = %e \n", f);
        }
        else if (b == 4)
        {
            printf("\nShag     G            Y            F\n"); //заголовки столбцов
            float v = 0;
            for (k = 0; k < 4; k++) //формирование массива
            {
                v = x1 += i;
                printf("%f ", v);
                for (l = 0; l < 3; l++)
                {
                    printf("%e ", mass[k][l]); //вывод массива
                }
                printf("\n");
            }
        }
        else
        {
            p = -10;
        }
        b = 0;
    }

    return 0;
}
