#include <stdio.h>
#include <math.h>

int main()
{
    int h;
    double j, f, g, y, i, x, a;
    printf("input x=");
    scanf("%lf", &x);
    printf("input a=");
    scanf("%lf", &a);
    printf("input h=");
    scanf("%d", &h);

    if (h == 1)
    {
        double g = -(10 * (18 * (a * a) + 11 * a * x - 24 * (x * x)) / ((-a * -a) + a * x + 6 * (x * x)));
        printf("G = %lf \n", g);
    }
    
    else if (h == 2)
    {
        double j = 21 * (a * a) - 34 * a * x + 9 * (x * x);
        if (j > -710.000001, j < 710.000001) 
        {
            f = cosh(j);
            printf("F = %e \n", f);
        }
        else
        {
            printf("Cosh nelzya vichislit");
        }
    }

    else if (h == 3)
    {
        double i = (3 * (a * a) - 25 * a * x + 8 * (x * x) + 1);
        if (i < 0)
        {
            printf("log ot otrichatelnogo chisla nelzya rasschitat");
        }
        else
        {
            y = log(i) / log(10);
            printf("Y = %lf \n", y);
        }
    }
    
    else
    {
        printf("Vvesennoe znachenie ne sootvetstvuet diapazonu");
    }
    
    return 0;
}
