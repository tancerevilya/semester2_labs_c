#include <stdio.h>
#include <math.h>

int main()
{
    double x, a;
    printf("input x=");
    scanf("%lf", &x);
    printf("input a=");
    scanf("%lf", &a);

    double g = -(10 * (18 * (a * a) + 11 * a * x - 24 * (x * x)) / ((-a * -a) + a * x + 6 * (x * x)));
    double f = cosh(21 * (a * a) - 34 * a * x + 9 * (x * x));
    double y = (log(3 * (a * a) - 25 * a * x + 8 * (x * x) + 1)) / log(10);
    
    printf("G = %lf \n", g);
    printf("F = %e \n", f);
    printf("Y = %lf \n", y);
    return 0;
}
